"//Airport.cpp"

#include "Airport.h" //header file that simulates the program for an airport
#include <queue> //tells the compiler to run the queue template class
#include <cstdlib> //tells the compiler to run in the standard library for C++
        //since it uses namespace std;
#include <iostream> //tells the compiler to put code from the header file called
        //iostream, which is a text file, into the program before creating the
        //executable

using namespace std; //tells you to define a scope in C++ and to group elements
//such as class, functions, and objects into one group in the C++ library

//the term void is used as a function return value that basically means the
//function returns nothing
void Airport(
        int landingTime, // Time segments needed for one plane to land
        int takeoffTime, // Time segs. needed for one plane to take off
        double arrivalProb, // Probability that a plane will arrive in
        // any given segment to the landing queue
        double takeoffProb, // Probability that a plane will arrive in
        // any given segment to the takeoff queue
        int maxTime, // Maximum number of time segments that a plane
        // can stay in the landing queue
        int simulationLength// Total number of time segs. to simulate
        ) 
{
    queue<int> takeoffQ, landingQ; //queue template class that is being run
    int landingWaits = 0, takeoffWaits = 0, timeFree = 0, crashes = 0;
    int numLandings = 0, numTakeoffs = 0; //variables that are going to be used
    //in the program - declared variables

    srand(time(NULL)); //initializing a random seed of numbers at any particular
    //second.

    for (int curTime = 0; curTime < simulationLength; curTime++)
        //current time is set for the start expression that is evaluated before
        //the loop begun.  the test expression is current time less than the
        //entire length of the simulation because it tests for what is true and
        //for what is false.  if the statement is true (curTime < simulationLength)
        //, then the body of the loop repeats. if false, then it stops. curTime++
        //is the count expression that is executed after each trip through the
        //loop that can increase or decrease by an increment of some value.
        
    { //body of the loop

        if (((double) rand() / (double) RAND_MAX) < arrivalProb) 
            // if the randomly generated number that is divided by the constant 
            //RAND_MAX that is defined in the <cstdlib> results being less than 
            //the probability of a plane landing before arrival time
        { //if statement is true 

            landingQ.push(curTime); //push the landing time to the current time

        }
        if (((double) rand() / (double) RAND_MAX) < takeoffProb) 
            //if the randomly generated number that is divided by the constant
            //RAND_MAX that is defined in the <cstdlib> results being less than
            //the probability of a plane landing any time before take off time 
        { //if statement is true

            takeoffQ.push(curTime); //push takeoff time to the current time

        }

        if (timeFree <= curTime) //if there is allocated time that no plane is
            //either taking off or landing, aka 'timeFree' is less than or equal
            //to the current time available then
        { //if statement is true

            if (!landingQ.empty()) //if the landing queue is not empty
            { //if statement is true

                if ((curTime - landingQ.front()) > maxTime) //if the following 
                    //statement of the result of subtracting the current time by
                    //the first landing being more than the maximum time allocated
                    //to stay in the landing area then it would result the following:
                {
                    crashes++; //the number of crashing planes
                    landingQ.pop(); //the landing would then be removed and 
                    //omitted of what the expected landing time would be
                } else //if the statement is other than true to the first if, then
               
                {
                    landingWaits += (curTime - landingQ.front()); //the time an 
                    //airplane has to wait to land after another has is equivalent
                    //to the time additionally to the current time minus the landing
                    //of the next plane.
                    landingQ.pop(); // omit the landing queue segment
                    timeFree += landingTime; //the allocated free time is equivalent
                    //to the time additionally to the landing time between planes
                    numLandings++; //incrementing the number of landings by 1 to
                    //keep track of the total number of landings
                }

            }
            else if (!takeoffQ.empty()) //if statement is not true to any of the
                //statements in the above body, then a separate statement is made
                //that explains if the takeoff queue is NOT empty, hence the ! 
                //before the takeoffQ.empty() which makes the statement NOT empty
            { //then the following is implemented

                takeoffWaits += (curTime - takeoffQ.front()); //the time an
                //airplane has to wait to land after another has is equivalent to
                //the time additionally to the current time minus the landing of
                //the next plane.
                takeoffQ.pop(); //omit the landing queue segment
                timeFree += takeoffTime; //the allocated free time is equivalent
                //to the time additionally to the take off time between planes
                numTakeoffs++; //incrementing the number of takeoffs by 1 to keep
                //track of the total number of take offs made by planes
            }

        }

    }

    while (!landingQ.empty() && (simulationLength - landingQ.front()) > maxTime)
        //a while loop is implemented to repeat the statement over and over as long as the
        //the statement thats the test condition is true.  in the test condition,
        //if the landingQ is NOT empty and the result of the length of the simulation
        //minus the beginning of the landing is greater than the maximum time allocated
        //is determined true, then the body of the loop is implemented
    { //body of the loop
        crashes++; //the number of planes that crash with increment of 1
        landingQ.pop(); // removes the landing queue
    }
    
    //the following below are outputs that are printed out when the test condition
    //in the above statement is false and the while loop is terminated.

    cout << "Number of crashes: " << crashes << "\n"; //outputting the number of crashes
    cout << "Number of takeoffs: " << numTakeoffs << " (avg delay " <<
            ((double) takeoffWaits) / ((double) numTakeoffs) << ")\n"; //outputting the
    //number of takeoffs by the results of takeoff waiting time divided by the number of takeoffs
    cout << "Number of landings: " << numLandings << " (avg delay " <<
            ((double) landingWaits) / ((double) numLandings) << ")\n"; //outputting the
    //number of landings by the results of landing waiting time divided by the number of landings
    cout << "Number of planes in takeoff queue: " << takeoffQ.size() << "\n"; //ouputting
    //the number of planes in takeoff queue with using the incrementation of the takeoffs
    cout << "Number of planes in landing queue: " << landingQ.size() << "\n"; //outputting
    //the number of planes in landing queue with using the incrementation of the landings

}
