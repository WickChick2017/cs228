// main.cpp //this is the header file used for C++

#include <iostream> //tells the compiler to put the header file called iostream
//which is a text file before the program makes the executable
#include "../Airport.h" //header file that simulates for an airport

using namespace std; //tells you to define a scope in C++ and to group elements
//such as class, functions, and objects into one group in the C++ library

int main() //main body of the program that is used for the simulation
{
    
    char x[128]; //defining data type variable character that holds one specific
    //type of data with 128 being the maximum number until returned false
    
    cout << "Starting the first simulation." << endl; //outputting the start of
    //the simulation with text into the c++ library

    Airport(5, 2, 0.1, 0.2, 45, 120); //numbers being implemented into the 
    //simulation to determine which condition is true or false

    cout << "First simulation finished (press enter to continue)" << endl;
    //outputting the first simulation completed and printing out 

    cin >> x; //inputting data type variable character x

    cout << "Starting the second simulation." << endl; //outputting the start of
    //the second simulation

    Airport(15, 10, 0.9, 0.9, 30, 1440); //set of numbers being implemented into
    //simulation to determine which condition is true or false

    cout << "Second simulation finished" << endl; //outputting the results for
    //the second simulation
    
    return 0; //return value
}

