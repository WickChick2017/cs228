"?// Airport.cpp

#include "Airport.h" //header file that simulates the program for an airport
#include <queue> //tells the compiler to run the queue template class
#include <cstdlib> //tells the compiler to run in the standard library for C++
        //since it uses namespace std;
#include <iostream> //tells the compiler to put code from the header file called
        //iostream, which is a text file, into the program before creating the
        //executable

using namespace std; //tells you to define a scope in C++ and to group elements
//such as class, functions, and objects into one group in the C++ library

//the following 
void Airport(
        int landingTime, // Time segments needed for one plane to land
        int takeoffTime, // Time segs. needed for one plane to take off
        double arrivalProb, // Probability that a plane will arrive in
        // any given segment to the landing queue
        double takeoffProb, // Probability that a plane will arrive in
        // any given segment to the takeoff queue
        int maxTime, // Maximum number of time segments that a plane
        // can stay in the landing queue
        int simulationLength// Total number of time segs. to simulate
        ) {
    queue<int> takeoffQ, landingQ;
    int landingWaits = 0, takeoffWaits = 0, timeFree = 0, crashes = 0;
    int numLandings = 0, numTakeoffs = 0;

    srand(time(NULL));

    for (int curTime = 0; curTime < simulationLength; curTime++) {

        if (((double) rand() / (double) RAND_MAX) < arrivalProb) {

            landingQ.push(curTime);

        }
        if (((double) rand() / (double) RAND_MAX) < takeoffProb) {

            takeoffQ.push(curTime);

        }

        if (timeFree <= curTime) {

            if (!landingQ.empty()) {

                if ((curTime - landingQ.front()) > maxTime) {
                    crashes++;
                    landingQ.pop();
                } else {
                    landingWaits += (curTime - landingQ.front());
                    landingQ.pop();
                    timeFree += landingTime;
                    numLandings++;
                }

            }
            else if (!takeoffQ.empty()) {

                takeoffWaits += (curTime - takeoffQ.front());
                takeoffQ.pop();
                timeFree += takeoffTime;
                numTakeoffs++;

            }

        }

    }

    while (!landingQ.empty() && (simulationLength - landingQ.front()) > maxTime) {
        crashes++;
        landingQ.pop();
    }

    cout << "Number of crashes: " << crashes << "\n";
    cout << "Number of takeoffs: " << numTakeoffs << " (avg delay " <<
            ((double) takeoffWaits) / ((double) numTakeoffs) << ")\n";
    cout << "Number of landings: " << numLandings << " (avg delay " <<
            ((double) landingWaits) / ((double) numLandings) << ")\n";
    cout << "Number of planes in takeoff queue: " << takeoffQ.size() << "\n";
    cout << "Number of planes in landing queue: " << landingQ.size() << "\n";

}
